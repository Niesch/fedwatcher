package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	feds := make(map[string]string)
	file, err := os.Open("addresses.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fields := strings.Split(scanner.Text(), " ")
		feds[fields[len(fields)-1]] = strings.Join(fields[:len(fields)-1], " ")
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	scanner = bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		ln := scanner.Text()
		who, ok := feds[ln]
		if ok {
			fmt.Printf("%s visited from %s\n", who, ln)
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

}
